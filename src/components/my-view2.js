/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css } from 'lit-element';
import {unsafeHTML} from 'lit-html/directives/unsafe-html.js';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

import PartisList from './partis.js';

class MyView2 extends PageViewElement {
  static get properties() {
    return {
    };
  }

  static get styles() {
    return [
      SharedStyles,
      css`
        #search {
          width: 300px;
          padding: 10px;
          border: 1px solid #afafaf;
          border-radius: 5px;
        }

        .partis {
          display: flex;
          flex-grow: 1;
          flex-direction: column;
        }
        .parti {
          margin-bottom: 10px;
          border: 1px solid white;
          border-bottom: 1px solid #cfcfcf;
        }
        .parti .header {
          padding: 15px;
        }
        .parti .header .info {
          border-sizing: border-box;
        }
        .parti[collapsed]:hover,
        .parti:not([collapsed]) {
          cursor: pointer;
          background-color: #efefef;
          border: 1px solid #afafaf;
        }
        .parti .header .info {
          display: flex;
          flex-flow: row wrap;
        }
        .parti .header .info .name {
          display: flex;
          justify-content: flex-end;
          text-align: left;
          flex-grow: 1;
          min-width: 120px;
          color: #333;
        }
        .parti:not([collapsed]) .header .info .name .triangle,
        .parti .header .info .name .triangleUp{
          display: none;
        }
        .parti:not([collapsed]) .header .info .name .triangleUp,
        .parti .header .info .name .triangle {
          display: initial;
        }
        .parti .header .info .name:hover {
          color: #333;
        }
        .parti .header .info .logoLink {
          display: flex;
          flex-grow: 0;
          margin-right: 10px;
        }
        .parti .header .info .logoLink .logo {
          width: 185px;
          height: 50px;
          max-width: 185px;
        }
        .parti .descShort {
          width: 100%;
        }
        .parti .descLong {
          display: flex;
          flex-grow: 1;
          flex-direction: column;
          padding: 15px;
        }
        .parti[collapsed] .descLong,
        .parti:not([collapsed]) .descShort {
          display: none;
        }
        .parti .descLong .link {
          display: flex;
          flex-grow: 1;
          justify-content: flex-end;
        }
      `
    ];
  }

  render() {
    return html`
      <section>
        <h2>Les partis politiques suisses</h2>
        <p><input id="search" placeholder="Recherche…" @keyup="${this._search}"></p>
        <div class="partis">
          ${this._filteredPartis.map( (parti) => {
            let desc = PartisList.content[parti.key] ? PartisList.content[parti.key] : "Information manquante";
            let descShort = desc && desc.length > 250 ? desc.slice(0, 250).replace(/##/g, "") : desc;
            let descHTML = PartisList.content[parti.key] ? this._convertMD(PartisList.content[parti.key]) : desc;
            let logoURL = parti.logo ? parti.logo : "https://image.flaticon.com/icons/svg/813/813020.svg";
            if(!parti.logo) {
              parti.logoWidth = "50px";
            }
            return html`
              <div class="parti" key="${parti.key}" name="${parti.name}" collapsed>
                <div class="header" @click="${this._toggleParti}">
                  <div class="info">
                    <div class="logoLink">
                      <a href=${parti.link} target="_blank">
                        <img class="logo" style=${parti.logoWidth ? `width:${parti.logoWidth}` : "" } src="${logoURL}"/>
                      </a>
                    </div>
                    <h2 class="name" title="${parti.name}">
                      ${parti.name}
                      <span class="triangle">▼</span>
                      <span class="triangleUp">▲</span>
                    </h2>
                  </div>
                  <div class="descShort">
                    ${descShort}…
                  </div>
                </div>
                <div class="descLong">
                  ${descHTML}
                  <br>
                  <br>
                  <a class="link" href="${parti.link}" target="_blank">${parti.name}</a>
                </div>
              </div>
            `
          })}
        </div>
      </section>
    `;
  }

  static get properties() {
    return {
      _filteredPartis: Array
    }
  }

  constructor() {
    super();
    this._filteredPartis = this._filterPartis(PartisList.partis);
  }

  _toggleParti(e) {
    let target = e.currentTarget.parentElement;
    let partiName = target.getAttribute("name");
    let partiKey = target.getAttribute("key");
    if(target.hasAttribute("collapsed")){
      target.removeAttribute("collapsed");
      this._saveFirestore(partiName, partiKey);
    } else {
      target.setAttribute("collapsed", true);
    }

  }

  // Save clicks into firebase
  _saveFirestore(partiName, partiKey){

    // Check if the user entry exists or create it if not.
    // Then update the user `partiClicked` field.
    let userDocRef = window.DB.collection("users").doc(USER.uid);
    userDocRef.get().then( (doc) => {
      if(doc.exists){
        this._updateUserPartiClicked(doc.data(), partiName, partiKey);
      } else {
        console.log("User does not exists, creating it.");
        let data = {
          created: new Date(),
          updated: new Date(),
          partiClicked: []
        };
        let newDoc = window.DB.collection("users").doc(USER.uid).set(data);
        newDoc.then( (doc) => {
          this._updateUserPartiClicked(data, partiName, partiKey);
        });
      }
    });
  }

  _updateUserPartiClicked(data, partiName, partiKey) {
    data.updated = new Date();
    data.partiClicked.push({
      name: partiName,
      key: partiKey,
      time: new Date(),
    });
    window.DB.collection("users").doc(USER.uid).set(data);
  }

  _convertMD(text) {
    let converter = new showdown.Converter({openLinksInNewWindow: true});
    let converted = converter.makeHtml(text);
    return unsafeHTML(converted);
  }

  _search(e) {
    let value = e.currentTarget.value.toLowerCase();
    this._filteredPartis = this._filterPartis(PartisList.partis, value)
  }

  _filterPartis(partis, search){
    partis = partis.slice(0);
    if(search){
      partis = partis.filter( (parti) => {
        return parti.name.toLowerCase().indexOf(search) > -1;
      });
    }
    return partis.sort( (a, b) => {
      if(a.name.toLowerCase() < b.name.toLowerCase()){
        return -1;
      }
      if(a.name.toLowerCase() > b.name.toLowerCase()){
        return 1;
      }
      return 0;
    })

  }

}

window.customElements.define('my-view2', MyView2);
