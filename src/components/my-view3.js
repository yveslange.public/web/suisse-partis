/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';
import { addToCartIcon } from './my-icons.js';

class MyView3 extends PageViewElement {
  static get properties() {
    return {
    };
  }

  static get styles() {
    return [
      SharedStyles,
      css`
      `
    ];
  }

  render() {
    return html`
      <section>
        <h2>Information</h2>
        <p>Pour toute demande de modification merci de contacter partissuisses (@) gmail.com</p>
        <p>
          Face à la diversité des sites web existants,  il a parfois été très
          difficile de trouver l'information. Je m'excuse si certaines
          imprécisions peuvent apparaître (auquel cas, merci de me contacter).
          J'ai tenté de regrouper et d'exposer ce qui a été mis à la
          disposition du publique.
        </p>

        <h2>Donations</h2>
        <p>
          Toute donation peut être effectuée via paypal ou en bitcoin (me contacter pour d'autres moyens)
          <ul>
            <li><a href="https://www.paypal.me/yveslange">Paypal</a></li>
            <li>Bitcoin wallet: 1BJqSzdwzg6vQcpfUrZyeYfxmnKBCqmtA8</li>
          </ul>

        </p>
        <p>
          Les donations me permettent de maintenir à jour cette liste hors de mon temps de travail et/ou de maintenir l'infrastructure (nom de domaine, hébergement, bières...).
        </p>

        <h2>Donateurs</h2>
        <p>(vide)</p>

      </section>
    `;
  }

  constructor() {
    super();
  }

}

window.customElements.define('my-view3', MyView3);
