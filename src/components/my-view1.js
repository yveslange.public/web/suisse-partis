/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { html, css } from 'lit-element';
import { PageViewElement } from './page-view-element.js';

// These are the shared styles needed by this element.
import { SharedStyles } from './shared-styles.js';

import PartisList from './partis.js';

class MyView1 extends PageViewElement {
  static get styles() {
    return [
      SharedStyles,
      css`
        #sectionCounter {
          display: flex;
          flex-grow: 1;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          color: #888;
          margin-bottom: 10px;
          border: 1px solid #afafaf;
          max-width: 400px;
          flex-wrap: wrap;
          padding: 10px;
          border-radius: 5px;
        }
        #sectionCounter:hover {
          font-style: italic;
          cursor: pointer;
        }
        #sectionCounter #badge {
          height: 40px;
          width: 40px;
          background-color: var(--app-primary-color, black);
          color: white;
          display: flex;
          flex-grow: 0;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          margin-right: 5px;
          margin-left: 5px;
          border-radius: 20px;
        }

      `
    ];
  }

  render() {
    return html`
      <section>
        <h2>Partis suisses</h2>
        <p>
          Ce site regroupe les pages de présentation des différents partis
          politiques suisses en un point centralisé ainsi que quelques
          informations supplémentaires (liens web, logo, ...)
        </p>
        <p>
          <strong>Actuellement (2019-10-02)</strong>: je ne me suis concentré que sur les partis principaux et ceux de mon canton: vaud.
          Je serai ravis d'avoir un peu d'aide pour rajouter les autres partis des autres cantons !
        </p>
      </section>
      <section>
        <h2>Pourquoi ?</h2>
        <p>
          Aujourd'hui, de nombreux partis politiques existent en Suisse.  Au
          quotidien, il peut être difficile d'épelucher la quantité
          d'information mise à notre disposition sur la toile ou dans les
          différentes revues papiers.
        </p>
        <p>
          Certains sont même découragés et ne connaissent pas les acteurs
          politiques principaux de notre pays. Il s'agit pourtant de la
          première chose à connaître avant tous débats politiques.
        </p>
        <p>
          Afin de gagner un peu de temps et d'aider à avoir une vision
          d'ensemble, j'ai tenté de regrouper – de manière équitable – les
          différentes présentations trouvées sur les sites officiels des
          différents partis. Ceci afin de centraliser l'information en un même
          lieu et de rendre leur accès bien plus facile.
        </p>
        <div id="sectionCounter" @click="${this._goToPartis}">
          <div>Voir les</div>
          <div id="badge">${PartisList.partis.length}</div>
          <div>partis suisses répertoriés</div>
        </div>
      </section>
    `;
  }

  _goToPartis(){
    window.location = "/partis";
  }
}

window.customElements.define('my-view1', MyView1);
