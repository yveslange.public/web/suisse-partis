/**
@license
Copyright (c) 2018 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at http://polymer.github.io/LICENSE.txt
The complete set of authors may be found at http://polymer.github.io/AUTHORS.txt
The complete set of contributors may be found at http://polymer.github.io/CONTRIBUTORS.txt
Code distributed by Google as part of the polymer project is also
subject to an additional IP rights grant found at http://polymer.github.io/PATENTS.txt
*/

import { css } from 'lit-element';

export const SharedStyles = css`
  :host {
    display: block;
    box-sizing: border-box;
  }

  a {
    text-decoration: none;
    color: var(--app-header-selected-color);
  }
  a:hover {
    text-decoration: underline;
  }

  section {
    padding: 24px;
    background: var(--app-section-odd-color);
  }

  section > * {
    max-width: 860px;
    margin-right: auto;
    margin-left: auto;
  }

  section:nth-of-type(even) {
    background: var(--app-section-even-color);
  }

  h2 {
    font-size: 18px;
    margin: 2px 0px;
    margin-right: 10px;
    margin: auto;
    text-align: center;
    color: var(--app-dark-text-color);
  }

  pre {
    overflow: auto;
    background-color: #ddd;
    color: #444;
    padding: 5px;
    border-radius: 3px;
    white-space: normal;
    box-shadow: 1px 1px 5px #afafaf;
  }

  @media (min-width: 460px) {
    h2 {
      font-size: 18px;
    }
  }

`;
