
const partis = [
  {key: "jeunesse-socialiste", name: "Jeunesse Socialiste - JS", link: "https://www.juso.ch/fr/la-js/a-propos-de-nous/", logo: "https://www.juso.ch/wp-content/uploads/2018/01/xcropped-juso-stern-2.png.pagespeed.ic.zapOX-mS8r.webp"},
  {key: "parti-pirate", name: "PP - Parti Pirate", logo: "https://www.partipirate.ch/wp-content/uploads/sites/3/2015/06/ppLogo-st-fr-rgb-h90px.png", link: "https://www.partipirate.ch/identiteetvision/"},
  {key: "parti-socialiste", name: "PS - Parti Socialiste Vaudois", link: "https://www.sp-ps.ch/fr/parti/nous-sommes-le-ps", logo: "https://www.sp-ps.ch/sites/all/themes/sp_ps/logo_fr.png", logoWidth: "50px"},
  {key: "jeunes-udc", name: "Jeunes UDC", link: "http://judc-vaud.ch/a-propos/", logo: "http://judc-vaud.ch/wp-content/uploads/2018/04/cropped-judc_logo-300x300.png", logoWidth: "50px"},
  {key: "pdc-ouverture", name: "PDC ouVERTure", link: "https://www.pdc-vd.ch/le-pdc-vaud/", logo: "https://www.pdc-vd.ch/wp-content/themes/cvp/assets/images/logo_pdc.png", logoWidth: "50px"},
  {key: "vert'libéraux", name: "PVL - les vert'libéraux", link: "https://vertliberaux.ch/", logo: "https://vertliberaux.ch/dam/jcr:3409f8b7-de99-4da8-aedc-19ce8fbeb4b0/logo_fr_vertliberaux_RGB.jpg"},
  {key: "parti-ouvrier-et-populaire", name: "EP. Parti Ouvrier et Populaire (POP)", link: "http://www.popvaud.ch/electionsfederales2019/", logo: "http://www.popvaud.ch/wp-content/uploads/2017/11/logo_drapeau_moyen.jpg", logoWidth: "150px"},
  {key: "urgence-ecologique", name: "Urgence Écologique - nous même", link: "https://www.facebook.com/nousmm/", logo: "https://scontent.fzrh3-1.fna.fbcdn.net/v/t1.0-9/53786383_414127842750429_6482670357789540352_n.jpg?_nc_cat=105&_nc_oc=AQnbmYqmIPu5DxU-kWEqJtuMHip4GwhaanKZiMcMXMu1crih6KGdAel4Z7DLLbedqf0&_nc_ht=scontent.fzrh3-1.fna&oh=94a12edb9e0c036cbb12a6ab84b97024&oe=5E28E191", logoWidth: "50px"},
  {key: "parti-evangélique", name: "AdC / - Parti Evangélique (PEV)", link: "https://www.evppev.ch/fr/politique/programme/programme-politique/", logo: "https://www.evppev.ch/fileadmin/template/images/logo_fr.png", logoWidth: "50px"},
  {key: "plr-innovation", name: "PLR.innovation", link: "https://www.plr.ch/parti/vision/", logo: "https://www.plr.ch/fileadmin/templates/plr.ch/img/logo/logo_plr.svg"},
  {key: "démocratie-directe-spiritualités-et-nature", name: "Démocratie Directe, SpiritualitéS et Nature", link: "https://microtaxe.ch/?fbclid=IwAR0KQwFe4A0kql45YNv5HL0YWXWRSGtuDAXZsZ6IMPJS-1EU3kv4Jz17i2k", logo: "https://scontent.fzrh3-1.fna.fbcdn.net/v/t1.0-9/69140462_121603905864805_618376381822140416_o.jpg?_nc_cat=109&_nc_oc=AQlIdtZXYlmDzxjBFHI5b0gVp9HQ2oVt5ULcoT6SqyDrnw0i9xDGGDqsMMGjZL3gY7U&_nc_ht=scontent.fzrh3-1.fna&oh=d3a36a9116ee95caec168e1c72958b4a&oe=5E39B427", logoWidth: "50px"},
  {key: "action-nationale-démocrates-suisses", name: "Action Nationale-Démocrates Suisses", link: "https://www.rts.ch/play/radio/face-aux-petits-partis/audio/face-aux-petits-partis-michel-dupont-democrates-suisses?id=7051400"},
  {key: "udc", name: "UDC", link: "https://www.udc-vaud.ch/campagnes/", logo: "https://www.udc-vaud.ch/wp-content/uploads/sites/13/LOGO-UDC-VAUD.jpg", logoWidth: "80px"},
  {key: "verts", name: "Les Vert-e-s. Mouvement écologiste", link: "http://www.verts-vd.ch/", logo: "http://www.verts-vd.ch/wp-content/themes/verts-vd/img/verts-vd-logo.png"},
  {key: "union-démocratique-fédérale", name: "AdC / - UDF - Union Démocratique Fédérale", link: "https://www.udf-suisse.ch/de/accueil.html", logo: "https://www.udf-suisse.ch/fileadmin/template/images/logoFR.png"},
  {key: "jeunes-verts", name: "JVVD - Jaunes Vert-e-s vaudois-es", link: "http://www.verts-vd.ch/jeunesverts/", logo: "http://www.verts-vd.ch/jeunesverts/files/2015/06/logo_jvv.png", logoWidth: "124px"},
  {key: "plr", name: "PLR.Les Libéraux-Radicaux", link: "https://www.plr.ch/parti/vision/", logo: "https://www.plr.ch/fileadmin/templates/plr.ch/img/logo/logo_plr.svg"},
  {key: "pdc", name: "PDC - Parti Démocrate Chrétien", link: "https://www.cvp.ch/fr", logo: "https://www.cvp.ch/themes/cvp/images/pdc.svg", logoWidth: "50px"},
  {key: "pvl-jeunes", name: "PVL - les jeunes vert'libéraux", link: "https://jungegrunliberale.ch/fr/", logo: "https://jungegrunliberale.ch/wp-content/uploads/2018/07/logo_glp__chfr_web-2.jpg"},
  {key: "jlr", name: "JLR - Jeunes Libéraux-Radicaux", link: "https://www.jlrv.ch/parti/valeurs/", logo: "https://www.jlrv.ch/fileadmin/templates/plr.ch/img/logo/logo_jlr-vaud_web.png", logoWidth: "90px"},
  {key: "pbd-juste-milieu", name: "AdC / - PBD Le juste milieu", link: "https://www.bdp.info/schweiz/fr/actuel/laboussoledupbd/", logo: "https://www.bdp.info/data/uploads/schweiz/francais/images-campagnes/logo-pbd_petit.jpg", logoWidth: "100px"},
  {key: "pns", name: "PNS - Parti Nationaliste Suisse", link: "http://www.partinationalistesuisse.ch/#", logo: "http://www.partinationalistesuisse.ch/img/logo75x25.png", logoWidth: "125px"},
  {key: "ensemble-à-gauche", name: "EP. Ensemble à Gauche", link: "https://www.ensemble-a-gauche.ch/category/axes/", logo: "https://scontent.fzrh3-1.fna.fbcdn.net/v/t1.0-9/71206634_371295777156671_5439413417087598592_n.jpg?_nc_cat=110&_nc_oc=AQlV6DO_CV63DCP01nhf9WnczJwaqSgAiru0k-N0Czk7LogFlSUUDDdXh-rn0mOML7I&_nc_ht=scontent.fzrh3-1.fna&oh=b92619b02b82ca2060f012f4f58b21c1&oe=5DF7BA10", logoWidth: "50px"},
]

const content = {
  "plr-innovation": "Voir \`PLR.Les Libéraux-Radicaux\`",
  "pdc-ouverture": "Voir \`PDC - Parti Démocrate Chrétien\`",
  "ensemble-à-gauche": `
Le monde dans lequel nous vivons, travaillons, pensons, aimons et luttons a régulièrement connu des crises. Celles-ci n’ont été surmontées qu’au prix de guerres, d’effroyables destructions et d’une croissance économique continuellement relancée. Aujourd’hui, ce système se heurte aux limites de la planète : une croissance infinie dans un monde fini est impossible.

Pour se perpétuer, le capitalisme doit s’emparer des derniers territoires qui lui échappent, privatiser les services publics et transformer en marchandise tout ce qui est encore gratuit, jusqu’à nos vies dans ce qu’elles ont de plus intime. Production et rentabilité doivent sans cesse augmenter, il faudrait travailler plus pour gagner plus et consommer toujours plus. L’exploitation des humains et le gaspillage des ressources naturelles s’amplifient. Notre environnement subit ainsi des dégâts irréversibles, qui mettent en danger l’ensemble des espèces vivantes et donc l’avenir même de l’humanité.Alors que l’urgence est manifeste, il faudrait agir résolument pour mettre un terme à cette course suicidaire. Pourtant les décideurs et les décideuses – multinationales, banques, gouvernements – restent passifs devant la catastrophe. Les grandes puissances internationales, dont les États européens et la Suisse, intensifient leur course folle à l’enrichissement des plus fortuné·e·s par l’accaparement des terres et le pillage des ressources, en plongeant dans la misère et la violence les peuples de nombreux États du Sud.Cependant, quand il s’agit de faire des cadeaux aux plus riches, aux entreprises et aux banques en vidant les caisses des collectivités publiques, quand il s’agit de rogner sur les prestations sociales ou de remettre en cause les prescriptions sanitaires ou écologiques, considérées uniquement comme des « entraves injustifiées au commerce », alors, plus d’hésitation : la propagande est constante et les décisions fermes. Ainsi, le stress, le chômage et la précarité augmentent au gré de la concentration des richesses d’une infime minorité. Celle-ci impose ainsi ses intérêts au détriment de la justice sociale et de l’écologie pour toutes et tous. Nous voulons tracer d’autres voies économiques et politiques, construire une société plus responsable, plus sobre, plus solidaire et plus juste. Nous voulons défendre et étendre à tous les niveaux un contrôle démocratique sur les ressources disponibles, les infrastructures et les conditions de vie et de travail. Nous voulons redéfinir, de toute urgence, nos priorités et nos besoins fondamentaux et inventer ensemble un mode de vivre radicalement différent, capable de garantir un avenir aux générations présentes et futures. Nous voulons également donner plus de place à des dimensions essentielles de la vie – culturelle, artistique, affective, citoyenne – reléguées aujourd’hui à une place marginale, alors qu’elles pourraient être développées sans limites et sans péril pour le monde qui est le nôtre.

## TRAVAILLONS MOINS POUR VIVRE MIEUX
- Instauration immédiate de la semaine de 32 heures, sans perte de salaire et avec embauches compensatoires
- Vers la semaine de 20 heures à terme, pour le partage des richesses et la préservation de notre écosystème

Alors que le Parlement envisage d’augmenter la durée légale du travail, nous proposons une réduction immédiate du temps de travail à 32 heures, avec embauches correspondantes et sans perte de salaire pour les revenus inférieurs à 120 000 francs par an. À plus long terme, dans la perspective de construire une société écologiste, solidaire et débarrassée de la course au profit, nous défendons la diminution de la durée légale du travail à 20 heures hebdomadaires.Nous passons plus de la moitié de notre vie au travail, à produire des richesses essentiellement captées par une minorité. En Suisse, les 2 % les plus riches accaparent autant de ressources que les 98 % qui s’épuisent au travail. Avec une durée du travail parmi les plus élevées au monde, la Suisse est l’un des pays où les salarié·e·s rapportent le plus à leurs employeurs·euses. Cette pression à la productivité et au rendement enrichit les plus riches et altère notre santé et notre qualité de vie.La réduction du temps de travail à 32 heures, combinée à un salaire minimum de 4500 francs par mois, à un salaire maximum, à six semaines de vacances annuelles et à une retraite à 60 ans pour tou·te·s, mettra un terme au rythme de vie effréné auquel nous sommes soumis·e·s et permettra de récupérer du temps libre. Nous pourrons prendre soin de notre santé, nous consacrer à notre famille et à nos proches, prendre part à des engagements citoyens, associatifs ou culturels et profiter de nos loisirs. Elle contribuera aussi à la lutte contre le chômage provoqué par la prochaine crise économique. Cette baisse du temps d’activité professionnelle doit s’accompagner d’une amélioration des conditions de ce travail par le renforcement des droits et du contrôle de la production par les salarié·e·s.Réduire le temps de travail est aussi une mesure écologiste et anti-productiviste, car il s’agit aussi de produire moins pour préserver notre écosystème. Passer à 20 heures de travail hebdomadaires implique de réduire, voire supprimer la production de marchandises polluantes et inutiles telles que la publicité, les armes, les industries du luxe et la production d’engrais de synthèse ainsi que de partager équitablement le travail utile et nécessaire.Partager le travail est enfin un pas important vers une égalité réelle entre femmes et hommes. Les femmes sont en effet plus nombreuses à devoir accepter des temps partiels et des horaires flexibles, quand elles ne sont pas simplement exclues du marché du travail. La réduction du temps de travail favorise une meilleure répartition des tâches telles que l’éducation, les soins aux proches, le ménage, toutes ces tâches non rémunérées largement supportées par les femmes.

## POUR UNE DÉCROISSANCE JUSTE ET SOLIDAIRE
- Pour que celles et ceux qui ont pollué la planète et son atmosphère en assument les coûts
- Pour une écologie politique, non punitive, refusant la dégradation des conditions de vie et de travail des plus démuni·e·s

Partout dans le monde, des milliers de jeunes se sont mobilisé·e·s pour nous alerter sur l’urgence de la crise climatique et écologique. D’ici 2024, les émissions de gaz à effet de serre de la Suisse devraient diminuer d’au moins 50 % par rapport à leur niveau de 1990. Nous devons ensuite atteindre la neutralité carbone d’ici 2030, sans avoir recours à des mesures de géo-ingénierie ou de compensation. Pourtant, les projets actuels de révision de la loi sur le CO2 ne répondent aucunement à ces objectifs et ne permettent pas d’éviter un réchauffement de plus de 1,5 °C, seuil que le GIEC juge déjà dangereux.Au lieu de s’attaquer aux véritables causes du problème, les projets de révision de la loi sur le CO2 misent sur des mesures de compensation. Mais pour répondre à l’urgence, il ne faut pas compenser, il faut réduire. Les nouvelles taxes prévues s’appliqueraient uniformément à toute la population, sans tenir compte des différences de revenu, alors que la pollution liée à l’importation des biens de consommation ainsi que l’impact climatique de la place financière helvétique restent, eux, complètement ignorés. Pourtant, les émissions de gaz à effet de serre du secteur financier sont 20 fois plus importantes que celles de toute la population suisse. Et on se refuse à toucher à des multinationales suisses comme Glencore, faisant partie des 100 entreprises provoquant plus de 70 % des émissions de CO2 dans le monde, ou encore Vale, responsable des barrages dont les ruptures ont causé des catastrophes écologiques et humanitaires majeures au Brésil, en 2015 et en 2019. Il est illusoire de penser résoudre la crise climatique sans s’attaquer au système profondément injuste qui la sous-tend. La crise écologique est une conséquence inévitable du capitalisme, qui ne peut survivre sans produire et gaspiller toujours plus. Les taxes et autres solutions dites « vertes » prônées par les partis gouvernementaux renforcent les inégalités sociales et poussent la population à s’opposer à toute politique écologique.Nous devons sortir de cette logique capitaliste et défendre une politique basée sur le principe de justice climatique : toute mesure environnementale doit contribuer à réduire les inégalités sociales et conduire à une meilleure répartition des richesses. Le principe du pollueur-payeur doit être appliqué : celles et ceux qui causent et profitent des émissions de gaz à effet de serre doivent être mis prioritairement à contribution, et non pas ceux et celles qui en sont les victimes, sous quelque forme que ce soit.

## PERSONNE N’EST ILLÉGAL·E !
- Pour la liberté de mouvement et d’établissement de tous les êtres humains ainsi que la régularisation de toutes les personnes « sans-papiers »
- Pour le droit de chacun·e à mener une vie digne, sûre et sans discrimination

Chaque être humain possède une dignité égale et inaliénable. Pourtant, la Suisse mène une politique migratoire cynique et barbare, caractérisée par le racisme et la violation des droits humains. Alors que nous vivons dans un système où capitaux et marchandises se déplacent librement, il n’en va pas de même des hommes, des femmes et des enfants. À nos frontières et sur notre territoire, police et administration opèrent le tri entre les personnes pouvant ou non prétendre à une vie digne et à la sécurité. Nous refusons de cautionner ces divisions artificielles, qu’elles soient basées sur la nationalité, la couleur de peau, le parcours migratoire ou le degré d’intégration au marché du travail. L’égale dignité de chacun·e doit au contraire se concrétiser dans l’égalité des droits pour toutes et tous et dans l’égal accès à l’emploi, au logement et aux services publics (santé, éducation, etc.).Les aspirations de chacun·e à vivre une vie libre et auto-déterminée sont légitimes. Il n’est pas acceptable qu’une partie de la population étrangère soit exclue du marché officiel du travail et que ses enfants n’aient pas le droit à une formation complète. De même, il n’est pas tolérable que des personnes subissant des violences domestiques soient contraintes de rester marié·e·s à des ressortissant·e·s helvétiques sous peine de perdre leur permis de séjour.C’est tout·e·s ensemble que nous voulons faire société. Nous ne nous reconnaissons pas dans une citoyenneté à dimension variable. Personne n’est illégal·e : nous sommes favorables à la liberté de circulation et d’installation pour toutes et tous. Nous exigeons un accueil digne des personnes qui fuient les guerres, les régimes autoritaires ainsi que les crises économiques et climatiques. Nous demandons le retrait de la Suisse des accords de Dublin, qui permettent aux autorités de renvoyer automatiquement des milliers de réfugié·e·s par année, sans même entrer en matière sur leur demande d’asile. Nous revendiquons la régularisation collective des personnes dites « sans-papiers ». Le strict respect des conventions internationales relatives aux droits des femmes et des enfants doit prévaloir sur toute autre considération.
`,
  "pns": `
## Texte de campagne: Environnement & Immigration

La vision du PNS fait le lien entre l’immigration de masse et l’environnement !

Le programme de base du PNS, totalement censuré par les médias depuis sa création en 2011, était visionnaire. La situation actuelle, tant sur les questions sociétales qu'environnementales et sécuritaires, conforte notre parti dans la poursuite de sa ligne politique.

Notre ligne politique se situe idéologiquement dans la ligne de Matteo Salvini en Italie ou de Victor Orban en Hongrie, ainsi sur certains sujets de l'AfD en Allemagne.

### Surpopulation, surdensité, environnement

Le PNS, en tant que parti nationaliste, est par essence préoccupé par l'environnement de son pays. Nul besoin d’idéologie écolo- marxiste-mondialiste-immigrationniste pour cela.

L'immigration de masse annuelle démentielle enregistrée (hors asile, plus de 150'000 personnes/année en moyenne), a conduit notre pays à une surdensité/surpopulation démentielle de plus de 650 habitants au km2 habitable, c'est-à-dire la plus haute densité en Europe !

En chiffres moyens, le nombre quotidien d’utilisateurs de nos ressources d’énergies :

8,5 millions d’habitants sur 34% de notre territoire habitable
320'000 frontaliers, dont 180'000 Français
1 million de personnes de passage
Et ce sans compter les «requérants» d’asile et les sans-papiers.

L’impact de cette densité - tout le monde peut le constater par les bouchons sur nos routes -, par la multiplication des besoins en milieux urbains et ruraux tant en infrastructures routières qu’en logements et établissements publics (hôpitaux, écoles, etc.), par une pollution exponentielle, ainsi que par un gaspillage de nos ressources d'énergies (par ex. électricité, nappes phréatiques, sources).

On constate déjà une dégradation importante de notre qualité de vie/alimentation/santé, la disparition de notre paysannerie, la perte de nos repères traditionnels et culturels, l’insécurité, les coûts insupportables de la santé, ainsi que la confrontation permanente avec des ethnies ou religions non adaptables avec notre mode de vie et valeurs humaines.

Et plus grave, une immigration de masse qui va aussi nous empêcher de prendre le tournant des énergies renouvelables et non polluantes sans avoir recours à des ressources dangereuses comme le nucléaire de dernière génération.

Gouverner c’est prévoir. En cas de crise majeure mondiale (guerre, crise économique), la surcharge de population peut devenir un réel danger pour le peuple suisse qui va se retrouver chez lui minoritaire et menacé.

On peut déjà en mesurer la menace en temps de paix et de plein emploi par la proportion prédominante d’étrangers dans nos prisons. Sur ce sujet aussi, le principe de précaution doit aussi prédominer.

A titre de comparaison, notre voisine l’Autriche, au même profil que la Suisse, est deux fois plus grande, compte 300'000 habitants de plus sur un territoire habitable de 34% également, et cela sans centrale nucléaire !

L'Autriche est-elle en faillite pour autant ? Demande-t-elle de doubler sa population par l'immigration comme la Suisse? Evidemment non !

Question production énergies renouvelables, par exemple le solaire, sur 29 pays européens (Etude SES 2017), l'Autriche se trouve au 10ème rang avec 838 kWh et la Suisse au 25ème avec 206 kWh.

Le PNS propose donc de stopper ce train fou mondialiste et apatride. Il sera la voix sans concessions que vous porterez au Conseil National par votre vote compact de la liste No 23.

Tout est dit, bon vote !

### Contrôle aux frontières

Ne perdons pas le contrôle! Au vu de l’actualité récente dramatique, le PNS demande le retour du contrôle aux frontières confié au Département fédéral de la défense (DPPS), pour une exécution du ressort exclusif de l’armée. L’application, manifestement en dilettante, des accords de Dublin-Schengen par notre Gouvernement permet l’entrée au lieu de refoulement et l’installation ad aeternam de migrants en situation totalement illégale. Le PNS demande également un retour des visas pour tous les ressortissants originaires à la base de pays hors continent européen.

### Assurance maladie (LAMAL) - Stopper l'endettement des ménages - Cotisations raisonnables

Pour faire baisser les coûts de l’assurance-maladie, le PNS préconise de suivre la piste d’une sécurité sociale sur le modèle scandinave, qui fonctionne, et de déprivatiser cette assurance.

### Assurance vieillesse et survivants (AVS/AI) - Pour des rentes dignes

Le PNS propose que les rentes soient réadaptées, en rapport avec le coût de la vie, et permettent ainsi aux rentiers de vivre dignement comme la Constitution le prévoit. Actuellement, on en est loin. L’âge de la retraite doit être maintenu à 64 ans pour les femmes et 65 ans pour les hommes.

### Pour une agriculture de proximité et respectueuse de la nature

Nos agriculteurs et maraîchers doivent pouvoir vivre au juste prix du produit de leur labeur, sans être mis en concurrence déloyale par des accords internationaux dévastateurs. Les produits agricoles importés en compétition avec la production indigène devraient donc être plus taxés aux frontières.


## Programme de base du PNS

### Immigration - N'ATTENDONS PAS QU'IL SOIT TROP TARD

Le PNS maintient sa proposition de stopper à l'entrée de notre pays toute immigration d'où qu'elle vienne, et ce sans distinction d'origine ni de religion. La densité de la population de notre pays a atteint une proportion insupportable: 600 personnes au km², soit la plus haute d'Europe (pour comparaison, la France a une densité 5,5 fois moins élevée, de 97 h/km²). Rappelons qu'en 1900, notre pays comptait 3 millions d'habitants, sur la même surface, alors qu'aujourd'hui c'est une population de près de 9 millions qui occupe une superficie composée de seulement 34% de terres habitables!

Nos ressources naturelles sont mises à mal et ne sont pas extensibles. Notre approvisionnement en apport énergétique ne pourra, au vu de la démographie exponentielle due à l'immigration, se passer d'énergie polluante. Il devient dès lors impossible de concevoir notre développement en accord avec notre environnement naturel. Notre peuple risque de se retrouver privé de sa liberté fondamentale, celle de disposer de son espace vital.

### Loi sur l'asile - RESPECTONS NOTRE IDENTITÉ, NE DÉTRUISONS PAS L'HÉRITAGE DE NOS ANCÊTRES

La Suisse n'est pas une terre d'asile. Germano-celtique à son origine, son homogénéité de pensée est à la source de sa fondation. Elle a su traverser guerres et crises avec sagesse et combativité. Mais surtout, sa créativité lui a permis de tirer son épingle du jeu économique sans se pourvoir d'industries lourdes.

La Suisse n'est pas non plus une agence internationale de travail temporaire pour pays en faillites. Force est de constater que, sous son couvert humanitaire, l'immigration est à 99% économique. Cette immigration, en voie de déstabiliser notre pays et de dégrader la paix civile, doit impérativement être bloquée. En raison de cet afflux extraordinaire, les naturalisations doivent être stoppées et, tout comme pour l'immigration et l'asile, un moratoire d'au moins dix ans doit être imposé. Dans le cas contraire, on verra une majorité d'étrangers d'origine prendre les commandes politiques de notre pays au risque de détruire notre patrimoine culturel, social et chrétien, et empêcher ainsi notre développement naturel. Le Suisse ne sera pas un étranger chez lui.

### Religions - LE PRINCIPE DE SÉCURITÉ AVANT TOUT

Il est évident que n'importe quelle secte ou religion faisant preuve d'hostilité ou d'agressivité envers nos valeurs et nos traditions, ou comme l'Islam en premier en guerre contre elle-même, ne doit plus avoir la possibilité de pénétrer sur notre territoire. Le principe de précaution doit être activé. Par conséquent, l'engagement par l'armée ou les forces de police d'adeptes de ce type de secte ou religion doit être proscrit, car leur entraînement militaire ou paramilitaire, de même que leur accès à des données informatiques privées ou à toute autre information sensible, pourraient être utilisés facilement à nos dépens. Dès sa création, le PNS a tiré la sonnette d'alarme sur ce phénomène politico-religieux.

### Contrôle aux frontières - NE PERDONS PAS LE CONTRÔLE

Le PNS demande le retour du contrôle aux frontières qui, au vu de la gravité de la situation, doivent être placées sous la direction et la participation exclusive de l'armée. Même l'accord de Schengen-Dublin n'est pas appliqué par notre gouvernement servile au mondialisme, puisque les migrants en situation totalement illégale suivant ce même accord, pénètrent sur notre territoire et viennent se faire enregistrer dans les centres d'accueil alors qu'ils devraient être refoulés ! Le PNS exige aussi le retour des visas pour tout les ressortissants de pays non-limitrophes.

### Assurance Maladie (LAMAL) - STOPPER LE RACKET

Le PNS préconise pour l'assurance maladie une sécurité sociale sur le modèle scandinave pour les Suisses et pour les étrangers l'assurance privée!

### Via Sicura - ARRÊTONS DE NOUS TROMPER DE CIBLE

Concernant la norme pénale "Via Sicura", le PNS la déclare illégale, puisque sans fondement et criminalisant injustement le peuple Suisse. Depuis les années 1970-80, le nombre de morts sur les routes a constamment régressé. Les équipements de sécurité passifs et actifs des voitures ont permis cette baisse drastique. Les limitations de vitesse en Suisse, imposées d'abord en 1973, étaient dues au conflit israélo-arabe qui avait entraîné des restrictions sur notre approvisionnement en carburant. La deuxième limitation de vitesse a été provoquée par la soi-disant " mort des forêts ", escroquerie idéologique qui n'a jamais existé que dans l'esprit malade de certains Verts intégristes. On constate donc que les limitations de vitesse en Suisse n'ont jamais été le fruit d'une épidémie d'accidents mortels.

### Droit international dans la Constitution Suisse - RENDONS AU PEUPLE SUISSE SA LIBERTÉ

Notre Constitution Suisse et nos lois doivent à nouveau primer sur le Droit international et sur le Droit européen. L'article 5 alinéa 4 de notre Constitution Suisse doit être aboli. Sans une Constitution Suisse libérée de toute entrave juridique internationale, point de liberté!

### Le contrôle sur les prix - NE NOUS LAISSONS PAS ARNAQUER

Déjà en 2011, le PNS avait demandé que soit instauré un contrôle sur les prix. Depuis le retour à la parité entre l'euro et le franc Suisse, la situation a démontré de quelle façon le peuple était soumis à des prix largement surfaits. Les hauts salaires suisses n'expliquent pas tout, notre pouvoir d'achat étant grêvé par des loyers également surfaits dans les villes et leurs alentours, la surpopulation n'arrangeant rien, ainsi que par des taxes et des impôts onéreux, dont la première utilisation ne profite pas au peuple Suisse. (Asile, aide au développement, etc).

Les importateurs n'ont toujours pas suffisamment baissé leurs prix, on attend dès lors un véritable contrôle officiel sur ce sujet vital pour les ménages à faible revenu.

L'agriculture locale et traditionnelle, seule garante de notre santé et de notre indépendance alimentaire, doit être soutenue activement et privilégiée face aux imports de produit de provenance internationale, dont nous ne pouvons nous assurer de la qualité.

L'agriculture Suisse en général ne doit pas être mise en concurrence avec les produits étrangers de moindres qualités, qui eux doivent être plus sévèrement taxés.

### AVS - RENDRE À NOS AÎNÉS CE QU'ON LEUR DOIT

Concernant l'AVS, le PNS soutient toute augmentation des rentes et n'acceptera pas la perte de nos acquis sociaux et donc le relèvement de l'âge de la retraite pour les femmes à 65 ans. Nos anciens doivent pouvoir vivre dignement de leur retraite comme le prévoit notre Constitution Suisse!

Le PNS depuis 2011, visionnaire!
`,
  "pbd-juste-milieu": `
## Le PBD (Parti Bourgeois-Démocratique Suisse) est synonyme d’attitude progressiste.
Nous mettons en avant des solutions pragmatiques, et non le
profil politique de notre parti. Il ne sert à rien de faire du bruit.
Par contre, il faut faire preuve de courage et de solidité pour
élaborer des compromis et des solutions.
Nous refusons le manque de respect, la polarisation, la radicalisation et la xénophobie. Nous donnons voix à la raison et
au progrès. Nous sommes la réponse à la pensée unique qui
s’est installée à notre gauche et à notre droite. En Suisse, la
vraie question qui se pose en politique, ce n’est pas «à droite
ou à gauche», mais «aller de l’avant ou régresser ?». – Le PBD
s’engage, quant à lui, à aller de l’avant !

## libéral, mais de manière responsable
La Suisse a besoin de progrès bourgeois garantissant
qu’une économie et une société libérales puissent
oeuvrer ensemble de manière responsable.
L’avenir réside dans un pays moderne et progressiste au sein
duquel les individus et les entreprises se respectent mutuellement,
font preuve d’un degré élevé de motivation et de responsabilité
personnelle tout en assumant leurs responsabilités envers les plus faibles et face à l’environnement.
Notre pays exprime sa souveraineté par la solidarité et l’ouverture
au monde et ne la confond pas avec l’isolation et le repli.

## pour des générations futures
Notre politique tient compte des évolutions et des besoins
de la société. Elle fait face aux défis futurs et s’applique à
poser les bons jalons.
Nous nous engageons pour des conditions-cadres attrayantes
qui rendent possible et garantissent le succès d’une politique
responsable et bien pensée. La classe moyenne, les PME et
les arts et métiers sont à cet égard les piliers de la Suisse.
Nous nous soucions de la prospérité, afin que ses acquis puissent
également être utilisés en faveur des générations futures
de manière judicieuse, responsable et solidaire.
Nous nous basons sur les besoins d’une société moderne et
responsable, sur la tolérance et le respect ainsi que sur une
approche prévenante envers l’environnement.
Nous agissons de manière raisonnable et en faisant preuve
d’égard – c’est là notre réponse à une polarisation croissante
et délétère. Ce qui est au centre de nos priorités, ce sont des
solutions factuelles et capables de rassembler une majorité,
et non la mise en avant obstinée d’exigences inflexibles. Nous
agissons dans ce contexte avec respect, courage et sens des
responsabilités.
`,
  "jlr": `
À l’origine des valeurs que nous défendons se trouve la conviction que l’être humain est l’entité la mieux placée et la plus capable de décider quels sont ses intérêts et comment les garantir. Ainsi, les constructions sociales qui nous permettent de vivre en communauté – à commencer par l’État et son ordre juridique – doivent agir en respectant ce principe fondamental. Chacun doit donc pouvoir exercer pleinement ses libertés individuelles ; celles-ci ne s’arrêtant qu’au commencement de ses responsabilités personnelles face aux autres membres de la collectivité.

La liberté individuelle la plus importante est naturellement la liberté d’opinion et d’expression. Ainsi, chacun a droit à former ses propres idées, ses propres avis sans que quiconque ne puisse s’y opposer, car c’est précisément cette capacité qui fait de nous des êtres humains.

La première responsabilité de chacun est naturellement, face aux libertés auxquelles on peut prétendre, le respect et la tolérance des opinions et choix des autres individus. Ainsi, nul ne peut s’arroger le droit d’imposer aux autres ce qu’il refuserait de se voir imposer.

La deuxième liberté fondamentale dont chacun peut se prévaloir, c’est celle de formation et d’activité. Chaque individu doit pouvoir décider par lui-même ce qu’il souhaite apprendre ou étudier. Il n’est pas du ressort de la collectivité d’imposer aux uns ou les autres le choix un domaine en particulier ; son rôle se limite à soutenir et à conseiller.

La responsabilité qui découle évidemment du droit de choisir son activité, c’est le devoir de se prendre en charge et de subvenir à ses besoins au mieux de ses capacités. Il ne serait donc pas acceptable que l’on puisse invoquer sa liberté de choix – de formation, de métier, etc. – tout en réclamant durablement d’être entretenu par le reste de la collectivité.

Enfin, la troisième liberté essentielle et celle d’entreprise. Elle garantit à chacun de pouvoir créer et poursuivre un ouvrage ou une organisation, quel qu’en soit le but : économique, culturel, artistique, solidaire, politique, etc. C’est ce droit qui permet le plein exercice des libertés d’opinion, d’expression et d’activité ; c’est cette possibilité qui permet le développement de notre société.

Finalement, la dernière responsabilité fondamentale est celle qui découle de ses propres décisions : tout individu est tenu d’assumer ses choix. Il peut, selon la situation, avoir besoin de l’assistance de la collectivité, mais il n’a pas le droit de lui reprocher les conséquences de ses propres actes.

Enfin, au-delà de ces libertés et responsabilités individuelles, il demeure la responsabilité commune de ceux qui vivent en collectivité d’assurer ensemble le respect des droits et des devoirs de chacun et de soutenir ceux qui n’en ont pas le plein exercice en raison de leur jeunesse – et d’encourager leur développement – ou d’un handicap physique ou mental – et de les accompagner et les aider. Parce que c’est par nos libertés et par notre respect des libertés des autres que nous sommes des êtres humains.
`,
  "pvl-jeunes": `
## ÉGALITÉ
Nous nous engageons pour une société égalitaire et une Suisse où le respect, la solidarité, la responsabilité et l’équité sont des faits. Le mariage pour tous et les modèles de travail flexibles sont, pour nous,essentiel d’une société libérale.

## SUCCÈS
Nous voulons une Suisse qui connaît ses forces et les développe intelligemment. Nous nous engageons donc pour une économie libérale où la concurrence, l’innovation et l’esprit d’entrepreneuriat sont encouragés. Nous nous positionnons également pour une justice générationnelle où l’âge de la retraite est considéré de manière adaptée à la réalité actuelle.

## PROGRÈS
Nous pensons d’ores et déjà au futur. Nous voulons une Suisse qui se positionne en faveur des prochaines générations. Nous nous engageons pour les énergies renouvelables, la création d’emplois sur notre territoire et la protection de la sphère privée, notamment sur internet.

## SOCIÉTÉ OUVERTE
Nous nous engageons pour une culture d’opportunités. Ainsi, nous luttons pour une égalité des chances à l’école car nous sommes convaincus qu’une bonne éducation est la base d’un individu fort, innovant et intégré. Nous appelons à une intégration rapide des réfugiés reconnus. De plus, nous encourageons une politique libérale en matière de drogues priosant ainsi l’intégration sociale et la santé de l’individu.
`,
  "pdc": `
## 1. Politique familiale et revenu net disponible pour vivre dignement
### 1.1 Une et plurielle, la famille moderne doit être soutenue
Développer une politique familiale plus ambitieuse et mieux coordonnée au niveau suisse.
Soutenir une conception de la famille moderne et sans discrimination.
Lutter contre la pénalisation fiscale des couples.1
Permettre à chaque couple d’aller au bout de son projet familial c’est-à-dire éviter qu’elles renoncent à avoir un enfant en raison de trop grandes pressions (financière, professionnelle).
Renforcer le pouvoir d’achat des jeunes familles, par des mesures ciblées : défiscalisation des allocations familiales, soutien à la santé, primes d’assurance gratuites pour les enfants, aides aux études.
Introduire la gratuité des primes d’assurance-maladie pour les enfants.
Aider les familles en difficulté, notamment monoparentales.
Combattre la pédocriminalité & le cyber-harcèlement des enfants (grooming).
### 1.2 La force du revenu net disponible, reconnaissance de l’effort
Revoir à la baisse la fiscalité des personnes physiques pour alléger la charge de ceux qui créent de la valeur de par leurs efforts.
Soutenir la classe moyenne active au travers de mesures récompensant l’activité plutôt que l’assistanat.
Favoriser en particulier les jeunes couples avec enfants, ainsi que ceux qui sont juste au-dessus du seuil de s’assumer eux-mêmes.
### 1.3 Une solidarité intergénérationnelle
Consolider les liens entre générations et combattre l’isolement social.
Étendre les solutions urbanistiques telles que quartiers et immeubles mixtes.
Soutenir la possibilité pour les seniors de rester actifs.2
Valoriser le travail des proches aidants et tiers aidants.
### 1.4 Meilleure conciliation entre vie professionnelle et familiale
Flexibiliser les conditions de travail à tout niveau hiérarchique, pour pères et mères (temps partiel à 80%, télétravail, horaires flexibles).
Faciliter la réinsertion de parents au foyer dans la vie professionnelle (validation des acquis de l’expérience (VAE), formation en économie familiale et maternité, etc.).
Favoriser un congé parental plus long, étendu aux pères car cela renforce l’activité professionnelle des mères.
Assurer la mise à disposition de crèches et gardes de jour de qualité, en nombre suffisant et à un coût abordable.
## 2. Santé et sécurité sociale assurées
### 2.1 Une maîtrise des coûts de la santé sans réduire la qualité des soins
Optimiser le fonctionnement du système de santé (25% plus cher qu’en Suède ou Autriche), pour mieux maîtriser les coûts de la santé sans toucher à la qualité des soins(initiative PDC).
Créer un organisme fédéral qui enregistrerait toutes les factures médicales pour établir une base de données permettant une analyse exhaustive des coûts.
Combattre la surconsommation médicale (examens redondants ou superflus, détection des incompatibilités médicamenteuses, « smart medicine »).
Revaloriser le rôle du médecin de famille pour un pilotage centralisé du parcours médical du patient.2
Renforcer l’ambulatoire et autres solutions (hôtel des patients) par rapport à l’hospitalier.2
Mieux planifier les besoins en matière hospitalière et inciter les hôpitaux à se répartir la tâche en se spécialisant.
Abaisser le prix des médicaments et encourager les médicaments génériques.
Faire baisser les primes d’assurance, en faisant pression sur les frais d’administration et les réserves des caisses d’assurance maladie.
Renforcer une bonne hygiène de vie dès l’école : prévention, activité physique, vigilance alimentaire.
Promouvoir la prévention.3
### 2.2 Une sécurité sociale équitable
Assurer le financement pérenne de l’AVS (priorité élevée); lancer des réformes structurelles compte tenu d’une meilleure espérance de vie.
Revaloriser les rentes AVS.
Flexibiliser l’âge de la retraite, permettre une baisse progressive du taux d’activité.2
Garantir le niveau de nos retraites du 1er et du 2ème pilier même en cas de mauvaise performance boursière.
Permettre aux personnes travaillant à temps partiel de se constituer plus facilement un 2ème pilier (en abaissant le montant de coordination).
Annuler les disparités entre classes d’âges pour les cotisations au 2ème
### 2.3 Un système pondéré luttant contre les abus
Revisiter l’AI et les prestations complémentaires en fonction des besoins.
Réprimer sévèrement les fraudes et lutter contre les abus.
## 3. Une politique climatique durable et une économie viable

### 3.1 Des réformes éco-responsables à tous niveaux : Etat, entreprises, citoyens
Œuvrer pour accélérer la mise en application du programme de la Stratégie Energétique 2050 de manière cohérente pour la Suisse : combler l’abandon de l’énergie nucléaire par des incitations efficaces à investir dans les énergies renouvelables indigènes, garantir l’approvisionnement en électricité, rentabiliser l’investissement dans l’énergie hydroélectrique, solaire ou éolienne.
Développer des infrastructures adéquates : transports publics performants, distribution d’électricité, transformation des déchets, traitement de l’eau…
Récompenser le comportement écoresponsable. Accompagner le geste citoyen et celui des entreprises : smart grid, isolement des bâtiments, formes non-polluantes de mobilité, tri et traitement des déchets (« Grüne Punkt »), chasse au gaspillage, etc.
Introduire des politiques transversales favorables à l’environnement par le développement de technologies propres, utilisant nos ressources de manière efficace et durable, garantissant un niveau d’emploi élevé en Suisse et par conséquent, renforçant notre place économique.
Combiner de façon harmonieuse le développement des réseaux ferroviaires et routiers.
Poursuivre l’effort de la LAT, en songeant à ses effets à long terme. Trouver de meilleures compensations pour les communes et les particuliers impactés.2
### 3.2 Une économie plus « verte » qui profite aux entreprises suisses
 Etendre les incitations à faire des économies de CO2 à toutes les entreprises, sur base volontaire : système des « conventions d’objectifs».2
Poursuivre vers une extension de l’économie circulaire (retravailler les déchets pour les transformer en matières premières).
Soutenir le transfert de technologies innovantes traitant d’environnement et faire de la Suisse un pôle d’excellence en la matière (Cleantech Valley).
Diminuer progressivement l’ensemble de la consommation des matières premières non-renouvelables et limiter la fabrication de produits dont la durée de vie estvolontairement courte.
### 3.3 Nos chercheurs et nos entreprises à l’écoute « multilatérale » de la planète
Mettre en œuvre et respecter les engagements pris lors de la signature de l’Accord de Paris, notamment en matière de réduction de gaz à effets de serre par des mesures concrètes telle que l’instauration d’une taxe sur les billets d’avion
Appuyer davantage nos EPF, Universités et HES à s’engager dans la recherche de solutions durables permettant d’offrir une réponse scientifique aux défis environnementaux d’aujourd’hui (Cleantech).
S’appuyer sur les avancées de la technologie pour développer une vraie diplomatie scientifique, recherche de solution globale par la technologie.2
Agir de concert avec la communauté internationale (crédits-carbone par exemple).
Rationnaliser les zones industrielles et commerciales pour une moindre utilisation des surfaces agricoles (densification), une meilleure insertion dans l’organisation régionales (rationalisation des transports), une autonomie énergétique.2
### 3.4 L’impulsion durable à travers la prise de conscience populaire
 Promouvoir une journée du climat pour une prise de conscience populaire des de l’impact de nos gestes quotidiens.3
Changer les évidences pour protéger l’environnement efficacement ; mettre en place des campagnes de communication ciblées.


## 4. Société innovante
### 4.1 Mise en valeur de l’audace et de l’esprit pionnier
 Développer un esprit pionnier qui encourage les idées audacieuses, l’acceptation de l’échec comme source d’apprentissage, un financement moins frileux.2
Favoriser l’axe : recherche fondamentale et appliquée, transfert de technologie, lancement de nouveaux produits. En faire des porte-drapeaux.
Créer des conditions favorables au développement des start-ups : défiscalisation des années de recherche, allégement administratif, accès facilité à des sources de financement adéquates.
Favoriser la croissance des start-ups et leur accès aux marchés internationaux.
### 4.2 Reconnaissance de l’effort de ceux qui prennent leur vie en mai
 Assurer une économie forte et une société à la pointe de l’innovation sociale.
 Mettre à disposition des Petites et Moyennes Entreprises (PME) les outils et le financement nécessaires pour s’adapter à un monde qui change. Les soutenir de façon ciblée.
 Appuyer davantage les jeunes qui se lancent par des bourses et du coaching.
 Inciter les entreprises à valoriser leurs travailleurs seniors et à transmettre leur savoir.
Considérer les programmes de réinsertion et de réintégration comme des investissements.
### 4.3  La formation, gage de meilleurs revenus
 Investir dans la formation et garantir à nos enfants des fondements solides pour leur avenir et celui du pays. Leur permettre ainsi d’être compétitifs sur un marché globalisé.
Maintenir un système d’éducation de qualité permettant à chacun de saisir sa chance.
Préserver le rôle fondamental de socialisation de l’école, dont la pratique des langues, la sensibilisation culturelle, l’intégration citoyenne.
Faire en sorte que chaque jeune soit orienté au mieux selon ses capacités, ses aspirations et les besoins de l’économie.
Renforcer le soutien aux Hautes Ecoles pour qu’elles restent compétitives sur le plan international.
Accompagner la capacité d’adaptation des travailleurs tout au long de leur vie en renforçant la formation continue2 et la validation des acquis.
Créer un 4ème pilier, c’est-à-dire un compte formation individuel, alimenté par les dépenses actuelles en formation continue, avec lequel chacun pourrait se recycler tout au long de sa vie, y compris ceux dépourvus de capacité d’épargne.2
### 4.4 Le pari de la numérisation
 Intégrer l’éducation digitale dans les cursus scolaires obligatoires (dès le cycle primaire) et des cours de mise à niveau pour les moins jeunes.
 Développer des pôles de compétences en matière numérique : blockchain, intelligence artificielle, drones et cybersécurité2
Soutenir la robotisation, l’internet des objets, l’industrie 4.0, afin de maintenir un haut niveau de performance économique tout en renforçant la formation continue.
Mettre en place une réflexion nationale sur les évolutions entre humains et numérique, anticiper les effets négatifs de la digitalisation et concevoir par anticipation des lois protégeant les travailleurs et la sphère privée (éviter un système intrusif).
 Créer des centres de données garantissant le secret numérique : la Suisse banque numérique.2
 Créer en Suisse un centre mondial de régulation d’internet et d’éthique numérique à Genève ; permettre que la Suisse contribue à façonner les standards internationaux.2
### 4.5 Des soutiens novateurs : à l’agriculture et aux services publics
Appuyer une agriculture bio de haute qualité qui minimise les apports chimiques.
Valoriser la production agricole suisse par un marketing professionnel.
Soutenir les administrations publiques et privées dans leur transition vers le numérique.
Exploiter les nouvelles technologies propices à l’allègement administratif.
Accorder des moyens financiers participatifs pour élargir les moyens de transport publics innovants et non polluants.
Poursuivre l’installation d’un réseau de télécom en fibres optiques, tout en veillant à la protection des populations sensibles aux ondes (5G).
Imaginer une société où l’agriculture côtoierait les start-ups dans un équilibre harmonieux et où des formes innovantes de redistribution permettraient de réduire certaines inégalités.


## 5. Une Suisse ouverte, connectée avec l’Europe et le monde
### 5.1 Une Suisse tirant le meilleur parti de sa situation en Europe
 Soutenir l’Accord Institutionnel (et la voie bilatérale), à condition de s’entendre sur : a) mesures d’accompagnement/protection des salaires ; b) aides d’Etat/fédéralisme ; c) accord sur le droit des citoyens. Ceci pour préserver un bon équilibre entre ouverture et souveraineté.
 Assurer les acquis de l’accord Schengen/Dublin (échange d’informations policières, gestion des demandes d’asile).
 Accepter de payer le milliard de cohésion (pays d’Europe moins développés), mais s’assurer du maintien de l’équivalence boursière et du programme de recherche 2021-2027 (Horizon Europe).
### 5.2 Une Suisse sûre et accueillante
Assurer notre capacité de défense et notre crédibilité militaire en investissant dans un nouvel avion de combat et les équipements qui vont avec.
 Renforcer notre cybersécurité, ainsi que la fiabilité et la sécurité des réseaux de télécommunication, afin que nos infrastructures critiques soient protégées.2
 Se doter des moyens nécessaires pour mieux se prémunir contre le terrorisme, la radicalisation, l’espionnage, les trafiquants de drogues et les réseaux du crime organisé. Renforcer la coopération internationale, les effectifs des services de renseignement et, selon les besoins, ceux des corps de police.2
 Poursuivre une politique migratoire qui tienne compte des intérêts économiques de notre pays et de ses valeurs ; renforcer la coopération internationale en matière de migration.
 Introduire un système des permis de travail pour des chercheurs et experts technologiques.2
### 5.3 Une Suisse impartiale, conciliatrice et multilatérale
Poursuivre sur la voie d’une Suisse ouverte au monde: pluralisme, libre échange, respect de l’Etat de droit.
Maintenir un taux minimal de 5% de l’aide publique au développement, ceci en lien avec le développement économique, la politique climatique et migratoire.
Participer à la réforme du système des Nations-Unies, pour le rendre plus efficace. Soutenir l’approche intégrée des ODD (objectifs de développement durable).
Continuer d’appuyer la Banque mondiale et les banques régionales pour la mise en place de solutions globales.
Protéger la réputation de neutralité, d’honnêteté et d’impartialité de notre pays. Et sur cette base, déployer son rôle de bons offices prodigués par la Suisse à travers le monde : notamment de médiateur entre belligérants.2
Renforcer l’engagement humanitaire de la Suisse et du CICR, en recourant davantage à la participation du secteur privé afin d’accélérer la relance des infrastructures vitales (eau, électricité) et la reconstruction des pays ravagés par la guerre.2
`,
  "plr": `
Si la Suisse est aujourd’hui considérée comme un modèle à succès, nous aurions tort de prendre ce succès pour acquis. C’est le fruit d’un travail acharné et le résultat de bonnes décisions – des pionniers libéraux-radicaux de 1848 à nos jours. La Suisse est une patrie pour toute personne ayant la volonté d’assumer sa responsabilité individuelle, tout en acceptant aussi d’en assumer en retour pour la collectivité. Elle permet à toutes et à tous de façonner leur destin avec confiance par l’assiduité, le respect et l’engagement. La Suisse est notre patrie. Il est donc de notre devoir – en tant que parti, en tant que Libéraux-Radicaux, en tant que pays – de préserver et de perpétuer ce modèle à succès. Ce n’est qu’ainsi que nous parviendrons activement à façonner l’avenir de la Suisse tout en préservant et en développant nos acquis.

[Plus d'information ici](https://www.plr.ch/fileadmin/documents/fdp.ch/pdf/DE/Partei/Zukunftsstrategie/20180929_FDP_Vision_DV_f.pdf)
`,
  "jeunes-verts": `
Les Jeunes Vert-e-s vaudois-es sont un parti politique s'engageant pour plus de justice sociale et pour développer un mode de vie durable.

Nous sommes un groupe de jeunes entre 14 et 29 ans, dont une vingtaine de membres très actifs. Nous nous réunissons régulièrement, pour différentes actions:

  - Nous participons activement à la récolte de signatures pour différentes initiatives, notamment notre initiative "contre le commerce de guerre".
  - Nous prenons activement part à des campagnes pour différentes votations, à travers la distribution de tracts, l'organisation de débats, etc.
  - Nous organisons un camp une fois par an avec les autres sections Romandes.
  - Nous organisons une journée "nettoyage intensif de la forêt" dans une forêt aux abords de Lausanne.
  - Nous organisons une journée de distribution de fruits et légumes de saison.
  - Nous participons à l'organisation du festival du film vert, en collaboration avec les autres sections suisses.
`,
  "union-démocratique-fédérale": `
##Principes et portrait de l'UDF
L'UDF est un parti politique à tendance sociale, indépendant de l'économie. Ses membres et sympathisants considèrent la Bible, parole de Dieu, comme fondement pour leurs diverses actions et prises de position.

##Fondements
Nous voulons prendre au sérieux nos responsabilités vis-à-vis de la société et de l'environnement. La politique ne doit pas servir l'abus de pouvoir. Nous nous appliquons, dans notre engagement politique, à rendre la population attentive à la parole de Dieu. Nous voulons considérer notre prochain comme supérieur à nous-même. Fidèles à notre identité, nous ne pouvons donc pas approuver des concepts et des comportements inadéquats. Partout où cela est possible, nous proposons des alternatives. Par notre engagement, nous aspirons à la guérison de la Suisse et de sa population, en premier lieu au niveau spirituel.

##Nous déclarons que
• l'intérêt général demeure prépondérant sur l'intérêt personnel et l'idéologie ;
• la justice élève un peuple ;
• la responsabilité individuelle sera promue et les charges équitablement réparties ;
• les problèmes seront résolus avec énergie et compétence ;
• les commandements de Dieu qui transcendent le temps seront courageusement mis en pratique.

##Public concerné
L'UDF est composée de chrétiens de différentes appartenances ecclésiastiques ayant une reconnaissance de principe de la foi chrétienne. Nos déclarations s'adressent à toute personne qui respecte les principes fondamentaux de la démocratie. Nous voulons être un partenaire de confiance qui pratique une politique de l'information objective et indépendante des intérêts économiques, permettant à nos membres et sympathisants de faire des choix politiques.

##Moyens
Notre travail est financé par des dons individuels, des donations pour des buts précis et les contributions de nos membres. Nous nous efforçons d'investir de manière responsable les moyens mis à notre disposition.
`,
  "verts": `
## En votant pour les VERTS…
- vous votez en faveur du climat. Car les VERTS font avancer la protection climatique à l’échelon international. Grâce à une économie verte, à plus de transports publics, d’espaces pour les piétons et cyclistes. Grâce à une agriculture « au naturel ». Grâce aux énergies renouvelables et à l’innovation.
- vous votez en faveur de l’égalité. Car les VERTS s’engagent pour un système de formation de qualité, une protection des salaires efficace et des rentes dignes. Pour l’égalité salariale et un congé parental adéquat. Pour le mariage et le droit d’adoption pour toutes et tous.
- vous votez contre le populisme mais pour une société qui tient à ses valeurs. Car les VERTS défendent une Suisse ouverte et solidaire. Militent pour les droits humains. Et encouragent la liberté individuelle dans le respect de l’autre.

Notre plateforme électorale développe ce que les VERTS entendent réussir – avec vous – lors de la législature 2019-2023.

\`\`\`
La Suisse a besoin d’une politique plus Verte… en faveur de l’environnement, du climat, d’une économie durable et de la justice sociale, en Suisse et dans le monde. Les élections fédérales du 20 octobre 2019 seront décisives.
\`\`\`

Les VERTS incarnent désormais l’espoir d’un avenir digne d’être vécu en Europe. Pour nous, la cohésion sociale, l’ouverture, la paix et la justice sociale sont tout aussi importantes que la protection de nos ressources naturelles.

### Pour la protection de l’environnement et du climat et une société solidaire

Actuellement, les VERTS sont le seul parti qui allie systématiquement questions environnementales et sociales, sous oublier les droits fondamentaux. Notre devise : « des actes, pas des mots ». Egalité entre femme et homme, mêmes droits pour chacun‑e, quelle que soit son orientation sexuelle ou son identité de genre (LGBTIQ*), collaboration entre personnes d’origines diverses relèvent pour nous de l’évidence.

### Ensemble avec la population et non avec les maîtres du monde

Les VERTS ne se laissent pas acheter par les assurances, banques ou autres représentant-e-s des maîtres du monde, qui déterminent la politique à Berne. Au contraire, nous travaillons avec la population, les organisations de la société civile ou de protection de l’environnement, les milieux de la culture et mouvements sociaux, les syndicats et entreprises responsables.

### Un mouvement international en plein essor

Membre des VERTS européens et mondiaux, nous faisons avancer la protection climatique et le tournant énergétique en Europe et au-delà de concert avec les VERTS des autres pays, luttons pour préserver la biodiversité et militons pour une économie circulaire responsable et solidaire.

Et nous sommes toujours plus nombreux et nombreuses : les VERTS volent de succès en succès en Suisse et en Europe. Le Vert fleurit et jaillit de partout, car de plus en plus de personnes l’ont compris : il faut enfin que les choses changent – pour la protection climatique et donc de nos ressources vitales, pour la protection de nos droits fondamentaux et de notre démocratie.

Voter Vert, c’est choisir un réseau de personnes engagées. Voter Vert, c’est choisir une Suisse qui a un avenir.
`,
  "udc": `
L’UDC est le seul parti...

• qui défend nos libertés

Nous nous battons pour un pays libre, affranchi de toute tutelle étrangère.
Les Suisses doivent pouvoir décider eux-mêmes des lois qui régissent leur
quotidien.

Nous luttons contre une dépendance étatique totale qui porte préjudice
à nos libertés. La force de notre pays réside dans nos concitoyens, libres et
responsables, et l'État doit rester le plus discret possible.

• qui préserve le modèle suisse

Nous reconnaissons le peuple suisse comme autorité suprême et affirmons
notre indépendance face a tout autre Etat. Il est inacceptable qu'une
instance étrangère nous enlève notre liberté.

Nous protégeons nos frontières qui sont la condition nécessaire pour
que notre démocratie puisse exister. Sans territoire commun, il n'y a pas
de peuple.

• qui agit concrètement pour l'environnement
et sans taxes

Nous favorisons la production et la consommation de denrées alimentaires
locales et souhaitons réduire les importations au strict nécessaire.

En limitant l'immigration, nous évitons de surexploiter nos ressources et
de saturer notre territoire. Nous nous battons pour ménager des espaces
naturels et des surfaces cultivables suffisantes a nos besoins.

Nous nous opposons a toute taxe supplémentaire. Nous soutenons le
développement de solutions durables au moyen de nouvelles technologies.

• qui se bat pour notre sécurité

Un pays libre est avant tout un pays sûr. Notre parti se bat pour une
défense crédible qui dispose de moyens suffisants et efficaces. Nos forces
de l'ordre méritent notre respect et notre soutien indéfectible.

Nous devons retrouver le contrôle de nos frontières. Schengen est une
passoire, nous exigeons des contrôles plus stricts ainsi que le renvoi
effectif des profiteurs, des faux réfugiés et des étrangers criminels.

• qui donne la priorité aux Suisses

Notre parti se bat pour que les résidents suisses soient prioritaires dans
‘attribution des places de travail.

ll est irresponsable de financer le séjour de réfugiés économiques avec
‘argent des contribuables, alors que nos retraités ont souvent de la peine
a subvenir a leurs besoins.

Nous n‘acceptons pas que I’arrivée en masse d’étrangers augmente
l'insécurité, sature nos infrastructures, densifie nos agglomérations et
exerce une pression démesurée sur les salaires et les places de travail.
`,
  "action-nationale-démocrates-suisses": `
"L’intégration ne fonctionne plus". Vous voterez pour les Démocrates suisses si vous estimez qu’une société multiculturelle est utopique et dangereuse et si pour vous, la Suisse est surpeuplée à cause des étrangers.
En revanche, vous ne voterez pas pour les démocrates suisses si la loi antiraciste est pour vous nécessaire, ou encore si vous ne voulez pas un retour des contingents avec les pays de l’Union Européenne. Michel Dupont nous explique le programme de son parti.
`,
  "démocratie-directe-spiritualités-et-nature": `
## Enfin + libres !

La démocratie économique libère toutes les énergies bénéfiques
avec
\`« Démocratie Directe, SpiritualitéS et Nature. »\`

Une micro-taxe de un pour mille sur tous les flux financiers, surtout ceux qui actuellement ne sont pas taxés, et une reprise en main publique des créations monétaires, remplace automatiquement tous les autres impôts et supprime toutes les déclarations fiscales personnelles et d’entreprises, y compris la TVA !

Sur notre logo, la micro-croque de la pomme, au bout de la flèche, symbolise 1 pour mille de la pomme, donc de toutes les masses monétaires en mouvement par électronique.

Visons les pommes et pas les enfants !

De petites miettes de la surabondance suffisent pour assurer le bien commun de tous !

##Buts visés :

Obtenir enfin une vraie transparence des transactions, des taxations des créations monétaires et des flux notamment spéculatifs non-taxés, etc
Automatiser une micro ponction en pour mille, une miette, sur toutes les transactions effectuées dans les systèmes de paiements électroniques
Supprimer toutes les déclarations d’impôts pour les personnes physiques et morales devenus inutiles et les procédures inquisitrices.
Garantir ainsi largement les recettes de l’Etat fédéral, des cantons, des communes et du social.
Diminuer les coûts d’exploitation des services fiscaux.
Garantir une démocratie économique directe et supprimer ainsi la TVA anti-sociale, l’évasion et l’optimisation fiscale injuste.
Baisser ou même supprimer les primes des assurances sociales, par exemple notamment celles de la LAMAL.
Inspirer le droit international
Dans le même sens par Maurice Allais, prix “Nobel d’économie”  … « Il s’agit de réformes fondamentales qui intéressent la vie de tous les jours de millions de citoyens. Ces réformes indispensables n’ont été réalisées, ni même envisagées, ni par les libéraux justement préoccupés de favoriser l’efficacité de l’économie, ni par les socialistes justement attachés à l’équité de la distribution des revenus… Les uns et les autres n’ont cessé d’être aveuglés par la répétition incessante de toutes parts de pseudo vérités et par des préjugés erronés. ..» .

Il existe près de 10  pays sans impôts. Monaco, les Bahamas, les îles Cayman, les Maldives, St Kitts et Nevis..

Nous travaillons aussi pour faire une BNS 3.0  ou https://aaapositifs.ch/

Il existe plusieurs pays qui ont des modèles fiscaux plus généreux pour leur population, leur Souverain à la suisse.

Étudiez attentivement une seule page A4, notre initiative-clé de transformation possible rapidement de tout le système fiscal suisse et qui tient compte des progrès technologiques,
sous la forme d’une suggestion générale.
Une seule page qui remplace des milliers de pages trop compliquées qui datent du passé.

`,
  "parti-evangélique": `
Le PEV regroupe des personnes qui, dans leurs activités politiques et leur engagement personnel au sein des différentes autorités, se laissent guider par les principes de l’Evangile. C’est d’ailleurs inscrit ainsi dans ses statuts.

Ces principes éthiques de la Bible représentent le fondement de notre société. Les valeurs chrétiennes telles que l’honnêteté, l’amour du prochain, la solidarité ou la paix représentent le socle socio-historique sur lequel repose la vision des droits de l’Homme, de la durabilité et de la justice sociale.

Ces valeurs ont été retenues par le PEV dans son Programme fondamental. Le parti cherche à établir un équilibre des forces, à agir en tant que médiateur, à aborder les problèmes négligés et à élaborer des solutions porteuses d’avenir. Il perçoit l’Etat comme un organe imparfait qui doit protéger le citoyen sans l’étouffer, le soutenir sans affaiblir sa responsabilité individuelle. Le PEV oriente sa politique en vertu des convictions suivantes :

- Dieu créa l’homme à son image. Nous sommes donc amenés à protéger et à respecter la vie humaine de la conception à la mort.
- Dieu a confié à l’homme sa création. Nous devons gérer les ressources naturelles avec soin et protéger l’environnement en pensant aux générations futures.
- Dieu créa le lien indestructible entre l’homme et la femme. Voilà pourquoi nous soutenons mariage et famille et protégeons les valeurs familiales.
- Au travers de son amour, Dieu créa une nouvelle réalité. Ainsi, nous nous exerçons dans l’amour de notre prochain et nous engageons pour un équilibre social et pour une solidarité internationale.

Ce programme de parti concrétise ces principes fondamentaux en décrivant la situation actuelle, les principes de base et les revendications du PEV pour les principaux thèmes politiques. Les revendications sont formulées de manière pragmatique et sont concrètement applicables. Ce programme détaille le positionnement du PEV et constitue un guide pour ses membres actifs et passifs.

Il fut adopté le 28 juin 2014 par l’assemblée des délégués du PEV Suisse à Ittigen (canton de Berne).
`,
  "parti-ouvrier-et-populaire": `
Comme la richesse sociale est fondée sur le travail, celui-ci est pour nous un droit fondamental pour tous les êtres humains. Chaque travail est important et a la même valeur. Nous voulons supprimer l’exploitation de l’humain par l’humain. Le PST/POP défend l’instauration d’un nouveau Code du travail, avec notamment l’institution d’un Tribunal du travail.

Concrètement, nous revendiquons :

- un droit constitutionnel au travail
- l’introduction d’un salaire décent d’au minimum  4500 francs par mois (24.75 francs de l’heure) et d’un salaire minimum pour les apprenti-e-s
- l’introduction d’un salaire maximum
- l’introduction obligatoire d’un 13ème salaire pour l’ensemble des travailleurs/euses
- l’introduction de la semaine de 35 heures, sans réduction de salaire, mais avec engagement de personnel
- un temps maximum de 8 heures de travail journalier
- imposition de la plus-value des profits de l’industrie 4.0
- aucune déréglementation des horaires des magasins. Une harmonisation à l’échelle nationale peut se faire seulement à condition de limiter les ouvertures des magasins pour un maximum de 11 heures par jour
- l’abolition de la précarité, c’est-à-dire l’établissement d’une véritable protection contre le licenciement pour toutes et pour tous, le droit à la réintégration et l’interdiction du travail intérimaire, du travail sur appel et du salaire au mérite
- le droit à la formation professionnelle continue financée par les entreprises
- le renforcement de la protection de la santé et de la sécurité au travail, notamment en renforçant les contrôles sans préavis et les sanctions vis-à-vis des entrepreneurs et des entrepreneuses
- l’obtention du droit de se réunir en assemblée du personnel pendant le temps de travail, d’élire des délégués et déléguées syndicaux d’entreprise et le droit de grève étendu, ainsi qu’élire des inspecteurs et inspectrice de sécurité sur le lieu de travail au sein de l’entreprise
- sanctionner financièrement et pénalement les entreprises qui ne respectent pas l’égalité salariale entre les hommes et les femmes
- l’interdiction de licenciements sans motifs valables
- l’interdiction des bonus et des dividendes
- lutter contre le dumping salarial et la sous-traitance
- l’interdiction des abus du statut d’indépendant par des entreprises du type Uber : les travailleurs et les travailleuses de ces entreprises doivent être considérés comme ses employé.e.s, avec les droits qui vont avec
- cesser toute forme de discrimination à l’embauche
- la reconnaissance du burn-out comme étant une maladie professionnelle
- l’interdiction des stages de longue durée qui ne sont pas rémunérés
- Et nous ne renoncerons pas…

avant que, dans la société socialiste, le travail ne serve plus à enrichir un petit nombre par l’exploitation des êtres humains et de la nature, mais à assurer le bien-être de toutes et de tous.
`,
  "vert'libéraux": `
##Pionniers : protéger le climat
Nous devons appliquer rapidement l'accord de Paris de façon cohérente. Pour y parvenir, nous avons besoin d'un objectif ambitieux de réduction des émissions de CO2 au niveau national. A l'étranger, il faut s'assurer que les émissions soient compensées selon des standards de qualité. Des mesures efficaces s'imposent. Outre les bâtiments, le trafic routier et aérien doit entrer dans l’équation. La transition écologique est possible et c'est une grande opportunité pour notre économie. Notre pays doit devenir un des leaders mondiaux de la protection du climat et des technologies propres.

##Site Cleantech numéro un
La combinaison de l'environnement et de l'économie est le principe fondamental des vert’libéraux. Une protection cohérente du climat et une stratégie énergétique durable sont de grandes opportunités pour notre économie. La transition rapide d'un système de subventions à un système se voulant incitatif rend les subventions et les nombreuses législations superflues. Un prix de l'énergie équitable améliore la rentabilité des mesures d'efficacité énergétique et assure le développement de technologies propres. Cela ouvrira aussi de vastes possibilités à notre industrie d'exportation.

##Défendre nos moyens d'existence
La biodiversité et la qualité de l'eau sont menacées. Le système actuel de paiements directs subventionne la pollution de l'eau potable et des sols. Des insectes et des oiseaux meurent. Nous devons nous réorienter progressivement ce système vers un système agricole durable, en évitant la surfertilisation et l'utilisation excessive de pesticides.

En effet, au lieu de continuer à promouvoir ces pratiques, les paiements directs devraient être supprimés pour les utilisateurs de pesticides à haut risque . Un objectif contraignant de réduction des émissions d'azote est également nécessaire.

##L'Europe comme une chance#
Nous voulons protéger et renforcer les valeurs communes de la Suisse et de ses partenaires européens telles que les droits fondamentaux et la démocratie. Nous voulons participer sur un pied d'égalité à la vie économique et être en mesure d'acquérir une formation reconnue partout en Europe. Les marchandises et les services doivent pouvoir circuler et s’échanger librement en Europe. Pour tout cela, il faut un accord-cadre. C'est pourquoi presque toutes les entreprises,  universités et associations professionnelles soutiennent également la suite naturelle des accords bilatéraux.

##Pour l'imposition individuelle
Le potentiel de main-d'œuvre est loin d'être exploité pleinement. De nombreuses personnes bien formées travaillent moins qu'elles ne le voudraient. La charge fiscale disproportionnée qui pèse sur le second revenu des couples mariés est une dissuasion grave qui entraîne de plus une pénurie de travailleurs qualifiés. La solution est l'imposition individuelle. Ce système permettra d'inciter la personne ayant le second revenu du ménage à gagner sa vie et ainsi d'accroître la participation des femmes au marché du travail et de contribuer à la prospérité de la Suisse.

##Mariage pour tous
On continue à refuser les droits du mariage civil à une partie de notre société. On ne leur offre la possibilité que de faire un mariage de deuxième classe sous la forme du partenariat enregistré. Ce n’est pas digne d’un état de droit moderne et plus du tout adapté à notre époque. La mise en œuvre de notre initiative « mariage civil pour tous » est un pas attendu depuis longtemps vers l'élimination de la discrimination des couples de même sexe et la création de droits et d'obligations égaux pour tous. C’est une évidence pour un pays libre.

##Rendre possible l'innovation
Le parlement adopte régulièrement des lois qui servent les intérêts d'industries ou d'entreprises individuelles plutôt que l'économie dans son ensemble. C'est le cas, par exemple, de la "Lex Booking.com" ou de la loi anticoncurrentielle sur les télécommunications, qui privilégie Swisscom, dont la majorité des parts appartiennent à l'Etat. L’économie de partage (« Sharing economy ») est également combattue à différents niveaux. Nous voulons lutter contrer ce protectionnisme. La digitalisation et la concurrence doivent enfin être reconnues par les politiciens comme une opportunité.

##Un service public efficace
De plus en plus d'entreprises publiques sont présentes dans le marché privé.

Qu'il s'agisse de la Poste, de Ruag ou de Swisscom, de fournisseurs d'énergie cantonaux, d'entreprises informatiques ou  de compagnies d'assurance du bâtiment. Cette évolution est problématique puisqu'il ne s'agit pas d'une concurrence à armes égales.

Grâce au secteur monopolistique, les entreprises publiques disposent d'un pouvoir de marché énorme alors qu'elles devraient se concentrer sur le service public proprement dit. Cela permettrait  aux entreprises privées innovantes de se développer de manière optimale.

##Infrastructures efficaces
La Suisse continue de dépenser des milliards pour ses infrastructures.

L'augmentation de la demande de transport est toujours résolue de la même façon: l'expansion du réseau existant.

L'argent est investi directement dans le bétonnage. Malgré cela, le réseau routier reste surchargé, même hors des heures de pointe. La solution est donc des systèmes modernes de gestion du trafic, des modèles flexibles de travail et d'école et des tarifs adaptés de mobilité. Cela permettra aux contribuables d'économiser de l'argent, de réduire l'impact sur l'environnement et d'augmenter le confort indidividuel.

##Bâtiment = centrale électrique
Nous voulons créer les conditions-cadres pour une alimentation électrique efficace et décentralisée ; il faut des réseaux intelligents au lieu d'une accumulation d'infrastructures inutiles. Les technologies sont disponibles depuis dix ans, mais la politique et les fournisseurs refusent d'ouvrir les yeux. On continue de faire comme il y a trente ans ! La digitalisation, en particulier, ouvre des dimensions  nouvelles dans l'utilisation de l'énergie, notamment en termes d'efficacité énergétique et de production décentralisée. Chaque bâtiment doit devenir une centrale électrique.

##Mobilité durable
Afin de réduire durablement les besoins de mobilité, nous voulons rapprocher les lieux d’habitat, de loisirs et de travail. Un aménagement du territoire cohérent et une tarification globale de la mobilité sont des enjeux capitaux. Il faut avant tout éviter le trafic, mais aussi promouvoir le passage à des moyens de transport plus durables, tels que le vélo, la marche à pied et les transports publics. L'électromobilité joue également un rôle clé et la réutilisation des batteries pour stocker l’énergie produit par un bâtiment est un complément à notre système électrique du futur.

##Moins d'étalement urbain
La densification des zones déjà construites est nécessaire à la préservation de zones de détente et verdure locales et à la création de nouvelles zones similiaires. Les agglomérations doivent continuer à être attrayantes grâce un paysage intact, et cela passe par une densification modérée. L'aménagement du territoire dans les zones périphériques doit tenir compte des préoccupations socio-économiques des zones de montagne et périphériques ainsi que de la protection des paysages en tant que zones de loisirs. Le tourisme respectueux de l'environnement doit également avoir sa place.

##Une agriculture moderne
Nous défendons une agriculture écologiquement durable, entrepreneuriale et multifonctionnelle. Pour y parvenir, les politiciens doivent repenser leur approche. Pour renforcer l'esprit d'entreprise et l'innovation et améliorer la qualité de l'environnement, nous avons besoin de moins, et non d’avantage, de ressources financières de l'État. Les incitations nuisibles à l'environnement doivent être éliminées.

Les services économiques et écologiques publics, tels que l'entretien du paysage culturel et la préservation de la biodiversité, devraient être au contraire mieux récompensés.

##Economie libérale
Nous considérons l'ordre économique libéral et la flexibilité du marché du travail comme l’atout majeur de la Suisse. L'esprit d'entrepreunariat doit être activement encouragé dans de bonnes conditions générales et ne doit pas être inutilement limité par la bureaucratie. Nos milliers de PME constituent un pilier important dans notre économie. L'intervention de l'État sur le marché devrait toujours reposer sur des systèmes d'incitation fondés sur le marché, tels que des prélèvements incitatifs et, si cela n'est pas possible autrement, sur des réglementations et des interdictions.

##Des finances saines
Nous voulons que l'État ne laisse pas des dettes aux générations futures. Des priorités claires doivent être fixées pour les dépenses. Nous soutenons à la fois une concurrence fiscale équitable entre les cantons et les communes et et le maintien de la péréquation financière. La concurrence fiscale favorise l'utilisation efficace des ressources limitées de l'État et conduit à une offre de services publics fondée sur les besoins.Nous sommes également de l'avis que les ressources et les activités des différentes zones de la Suisse nécessitent une solidarité entre les cantons et les régions.

##Une politique sociale juste
Nous défendons un filet de sécurité sociale qui permet une vie digne  dans toutes les phases de la vie. Cependant, la politique sociale ne peut rendre justice aux personnes que si elle est adaptée à leur potentiel.

Cela signifie, par exemple, que l'aide sociale doit aussi ouvrir la voie à une vie indépendante. En outre, la politique sociale n'est durable que si elle protège nos services sociaux pour les générations futures. C'est pourquoi nous rejetons la politique indifférenciée de l'arrosoir de prestations à gauche de même que les exercices d'économie tentés à droite.

##Prévoyance vieillesse durable
Nous vivons en bonne santé de plus en plus longtemps. Cela conduit nécessairement à une réforme de la prévoyance vieillesse. Nous voulons assouplir l'âge de la retraite et égaliser l'âge de la retraite pour les hommes et les femmes. Dans le même temps, nous devons améliorer le revenu à temps partiel dans le domaine du deuxième pilier en supprimant la déduction de coordination. Les charges doivent être réparties également entre les générations. Afin d'assurer cette équité intergénérationnelle, un frein à l'endettement est une nécessité pour la prévoyance vieillesse.

##Une société libre
Nous voulons vivre dans une société où la liberté individuelle et la responsabilité personnelle occupent une place primordiale. L'État ne devrait pas dicter aux gens comment ils devraient vivre et il devrait se comporter de façon neutre à l'égard de toutes les visions du monde, dans la mesure où elles sont compatibles avec notre système juridique. L'égalité des chances, l'égalité des sexes et de tous les modèles de vie et de famille sont pour nous une évidence et doivent être garantis. La discrimination et le racisme n'ont pas leur place dans notre société.

##100% d'égalité
L'égalité entre les sexes et tous les modèles de vie et de famille sont pour nous une évidence.  Il s'agit notamment d'assurer l'égalité de salaire pour un travail égal et l'égalité de traitement dans le droit fiscal et le droit de la sécurité sociale. L'ouverture du mariage aux couples de même sexe, la pleine égalité pour l'adoption et la médecine reproductive sont une évidence. De plus, la conciliation de la vie professionnelle et de la vie familiale doit être garantie en offrant des structures d'accueil quotidiennes des enfants à tous les niveaux.

##Renforcer formation et recherche
L'éducation et la recherche jouent un rôle clé dans une société libérale. En particulier, pour assurer l'égalité des chances et notre force d'innovation. Une école primaire forte et un système d'éducation duale sont primordiaux. L'apprentissage d'une deuxième langue nationale comme première langue étrangère à l'école primaire est important pour faciliter les échanges linguistiques et culturels entre les différentes parties du pays. Nous défendons également activement la compétitivité internationale de nos sites universitaires et de recherche.

##Réformer le système de santé
Nous voulons un système de santé garantissant des soins à toute la population tout en favorisant la responsabilité personnelle et la prévention afin de réduire les coûts. Cela comprend un système d'assurance maladie incluant la liberté de choix. Les mauvaises incitations doivent être supprimées.  Il faudrait,p. ex., un financement uniforme des services aux patients hospitalisés et ambulatoires. Il faut mettre fin au système incitant les médecins hospitaliers à recevoir le plus de patients possible. Les droits des patients, en particulier celui d'être informé, doivent être renforcés.

##Une Suisse ouverte et connectée
La Suisse est l'un des pays les plus interconnectés au monde. Nous y voyons une chance à saisir et nous nous engageons pour une politique active en faveur de la paix, de la démocratie, de l'État de droit, de la protection de l'environnement et des droits de l'homme. Le libre-échange et les partenariats économiques  assurent notre prospérité et, si les conditions sociales et écologiques appropriées sont réunies, offrent à tous les pays une possibilité de développement économique. Cela crée la stabilité, favorise la paix et réduit la pauvreté dans le monde.

##Intégrer avec succès
Nous sommes convaincus que la migration est profitable pour notre société et notre économie et que cela continuera à être le cas. Lorsque les gens s'installent chez nous, leur volonté d'intégration doit être une condition préalable. Le principe de la responsabilité personnelle s'applique également ici. Toutefois, l'État devrait soutenir l'intégration, par exemple avec des cours de langue. L'attribution de la nationalité doit se fonder en premier lieu sur le degré d'intégration et non sur la durée du séjour en Suisse.

##Pour la tradition humanitaire
Nous restons fidèles à la tradition humanitaire de la Suisse. Notre pays devrait continuer à offrir l'asile aux personnes persécutées.

Pour que cela continue, les procédures d'asile doivent être menées rapidement. Conformément à notre responsabilité humanitaire, nous nous engageons en faveur d'une coopération internationale au développement durable qui accorde une importance essentielle aux défis écologiques. Une politique de développement cohérente inclut également la renonciation aux subventions à l'exportation et l'accès des pays en développement au marché suisse.

##Pour les droits fondamentaux
Les droits fondamentaux tels que la liberté, l'intégrité individuelle et la propriété de tout être humain doivent être protégés. Lorsque la responsabilité personnelle et la décence font défaut, l'État a la tâche de protéger ces droits fondamentaux et de punir les infractions de manière proportionnée. Cependant, nous devons être conscients qu'il ne peut y avoir de sécurité absolue dans une société libre. Les mesures de sûreté de l'État exigent une base juridique et un contrôle efficace. Dans notre société libre, nous devons nous engager pour garantir la sphère privée.

##Service citoyen pour tous
Les forces armées doivent être adaptées aux menaces et dangers les plus vraisemblables à l'heure actuelle et dans le futur. Il s'agit du cyber-terrorisme, de la protection des infrastructures critiques, des services d'appui aux catastrophes environnementales ou des missions de maintien de la paix dans le cadre d'un mandat de l'ONU. Nous exigeons également un service citoyen obligatoire à la place du service militaire obligatoire actuel, à l'occasion duquel les femmes et hommes peuvent choisir librement  dans quel domaine (protection civile, service civil ou militaire) ils souhaitent servir.
  `,
  "jeunes-udc": `
Les Jeunes UDC Vaud constituent une formation politique qui a pour mission de
trouver des solutions aux problèmes d’aujourd’hui et de demain. Représentants
de la droite conservatrice, nous nous engageons notamment en faveur de la
liberté individuelle, de la souveraineté de la Suisse, de l’économie locale et
de l’agriculture ou encore du fédéralisme.

Notre section, affiliée aux Jeunes UDC Suisse, a vu le jour en 2003. Elle a
gagné en force au fil des années, jusqu’à s’afficher comme la première
formation de jeunes du Canton de Vaud lors des élections fédérales de 2011 et
de 2015, où plus de 2% des électeurs l’ont soutenue dans les urnes.

Nos activités sont diverses, allant de la présence sur les stands politiques
jusqu’à l’élaboration de réformes et modifications de lois, en passant par
l’expression continuelle de nos idées sur les réseaux sociaux ou lors de
débats publics.
`,
  "jeunesse-socialiste": `
Nous luttons pour un monde plus juste et nous nous engageons pour une Suisse
meilleure.

Nous mettons l’humain au centre de notre politique.

Nous voulons un système économique qui redistribue les richesses à la
population, offrant une place pour toutes et tous et qui respecte
l’environnement.

Nous luttons pour l’égalité.

Nous luttons pour une société dans laquelle tout un chacun a les mêmes chances
de développer ses talents .

Nous nous engageons pour une société dans laquelle personne n’est défavorisé en
raison de son origine sociale, de sa couleur de peau ou de sa sexualité.

Nous sommes la jeunesse du Parti socialiste.

Nous sommes indépendants•es et prenons seuls•es nos décisions.

Des jeunes de tous les horizons, comme les écoliers•ères, les jeunes
travailleurs•euses ou les étudiants•es sont actifs•ves à la JS.

Ensemble, nous planifions nos campagnes, nos actions politiques, récoltons des
signatures, manifestons et discutons pour trouver les chemins vers un avenir
meilleur.

Le monde ne se sauvera pas seul. C’est seulement grâce à notre engagement en
commun, que nous pourrons le voir changer.
`,
  "parti-socialiste": `
«La force de la communauté se mesure au bien-être du plus faible de ses
membres», peut-on lire dans la Constitution fédérale. Nous nous engageons pour
une société où tout un chacun peut s’épanouir librement, mais où il existe
aussi un filet social qui empêche qu’une personne se retrouve dans une
situation précaire. Le PS veut une économie au service de l’homme et non
l’inverse. Quiconque travaille en Suisse, qui est un pays riche, doit pouvoir
vivre de son salaire. Nous sommes convaincus que, pour avancer, il faut être
solidaire. La Suisse dispose d’un réseau social solide et d’infrastructures
publiques de qualité. Le PS y est pour beaucoup. Sans lui, il n’y aurait par
exemple pas d’AVS, pas d’assurance maternité et pas de droit de vote des
femmes.

\`\`\`
Depuis 125 ans, le PS s'engage en faveur d'une politique pour tous, sans
privilèges. Nous disons oui à une Suisse qui rassemble et non à une Suisse qui
exclut.
\`\`\`

Mais il ne faut pas en rester là. Nous sommes persuadés que la Suisse peut
devenir plus équitable, plus novatrice et plus progressiste.  Si nous misons
systématiquement sur les énergies renouvelables, nous protégerons notre
environnement et créerons des emplois novateurs et conformes au développement
durable. Si nous misons sur la formation et l’intégration, nous créerons des
perspectives et préviendrons le chômage. Si nous investissons dans la
construction de logements en coopérative, nous contribuerons à freiner la
spéculation foncière et veillerons à la mise à disposition de logements à des
prix abordables. Pour résumer : le PS s'engage en faveur d'une politique
destinée à toutes et tous et non à une poignée de privilégiés.
`,


  "parti-pirate": `
Le Parti Pirate propose une vision de la société qui donne au citoyen le rôle
politique central. La société citoyenne du Parti Pirate est établie sur les
principes de la démocratie participative.

Le citoyen est la pierre angulaire de notre démocratie. Il participe et
contribue librement à la vie publique. Le Parti Pirate offre les moyens au
citoyen pour lui permettre de s’engager dans une démarche active:

Le citoyen est en prise directe avec les membres et les élus du Parti Pirate.

  - Le citoyen reçoit le soutien d’un parti qui est le trait d’union entre les
  enjeux de la révolution de l’information et la société actuelle.
  - Le Parti Pirate donne les moyens au citoyen de se mobiliser et d’agir par
  l’action citoyenne.
  - Les valeurs du Parti Pirate sont la Justice, la Liberté, le Respect, la
  Responsabilité, le Partage et la Transparence. Elles prennent tout leur sens
  au sein d’une démocratie participative.

La vision Pirate trouve ses fondements dans la protection de la sphère privée,
la défense de l’Etat de droit dans l’espace numérique et la liberté
d’expression par la technologie. Ces enjeux ont révélé la nécessité d’adapter
la société en intégrant les nouvelles réalités de la société de l’information.
Une société de l’information qui serait construite principalement pour
surveiller ou contrôler les activités humaines ne permettrait pas au citoyen
d’y participer et d’y contribuer librement.  La vision du Parti Pirate de la
société est centrée sur le citoyen et s’organise autour de quatre piliers :

La société doit assurer dans l’espace citoyen la prévention liée particulièrement aux risques sociétaux dans les domaines du social, de la santé et de la sécurité.
  - L’environnement doit être préservé pour les générations futures et
  l’utilisation des ressources doit être durable.
  - Le patrimoine commun doit être géré et développé de manière solidaire tout
  en étant accessible à tous.
  - L’économie doit être résiliente et décentralisée pour assurer la libre
  entreprise tout en étant équitable.
  - Le citoyen doit disposer des meilleures conditions possibles, notamment
  grâce à une formation de qualité, pour contribuer à la société afin qu’il
  puisse faire face aux enjeux de la révolution de l’information et en saisir
  les opportunités. La démarche Pirate est déterminée par l’Etat de droit,
  l’éthique et les valeurs pirates (justice, liberté, respect, responsabilité,
  transparence).
  - La démarche est pragmatique et ouverte. Elle emprunte à l’utilisation des
  outils participatifs qui sont issus de la révolution numérique dans une
  approche non-technocratique. La plus grande efficience est recherchée pour
  formuler des solutions aux enjeux. La démarche repose sur les conditions
  suivantes :

Établir une vision claire des enjeux afin d’identifier les besoins.
  1. Proposer un cadre législatif dans lequel une solution peut être
  développée.
  2. Permettre au citoyen de s’approprier les moyens de se forger une opinion,
  de la défendre et d’adhérer aux projets de manière citoyenne, contributive et
  mobilisatrice.
  3. Les membres du Parti Pirate peuvent contribuer de manière active à tous
  les cycles de développement de la démarche pirate : les informations, les
débats et les comités sont toujours publics ce qui permet à chacun de
participer et d’avoir une vision transparente de la démarche pirate.
`
}

export default { partis, content }
